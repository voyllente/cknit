/**
 * Copyright @2019 Josin All Rights Reserved.
 * Author: Josin
 * Email : xeapplee@gmail.com
 */

#pragma clang diagnostic push
#pragma ide diagnostic ignored "OCUnusedMacroInspection"


#ifndef CKNIT_CK_TASK_H
#define CKNIT_CK_TASK_H

#include <ck_timer.h>
#include <config.h>
#include <list.h>
#include <estring.h>
#include <http_socket.h>
#include <event.h>
#include <exjson.h>
#include <pthread.h>

/**
 * @brief Introduction
 * Some global variables
 */
#define GLOBAL_VARS
GLOBAL_VARS EXJSON *tasks, *e_system;
GLOBAL_VARS QUEUE_LIST *tasks_fp, *tasks_mistake;
GLOBAL_VARS int         server_fd;

typedef struct _LIST_DATA{
    char *name;
    char *value;
    long  id;
} LIST_DATA;

void trash_file_exception(void *v);
LIST_DATA *INIT_LIST_DATA();

enum TASK_STATUS {
    TASK_ON = 0,
    TASK_STOP = 1
};

/**
 * @brief Introduction
 * Some name macros
 */
#define COMMAND "command"
#define PERIOD  "period"
#define STATUS  "status"
#define OP_DOWN "op_down"
#define FOREVER "forever"

#define SYSTEM    "system"
#define DAEMON    "daemon"
#define TASK_CONF "task_conf"

/**
 * @brief Introduction
 * Some macros for loop the Exjson structure
 * v's type is EXJSON_V
 */
#define EXJSON_FOR_EACH_OBJECT(e, v) do { int i, j; EXJSON *node; if (!e) break; for ( i = 0; i < E_NUM_P(e); ++i ) { v = E_DATA_P(e) + i;
#define EXJSON_ARRAY_EACH(e, v) do { int i, j; EXJSON *node; for ( i = 0; i < E_NUM_P(e); ++i ) { node = EV_VALUE_P(E_DATA_P(e) + i);
#define EXJSON_ARRAY_EACH_END() }} while(0)
#define EXJSON_FOR_EACH_ARRAY(e, v) do { int i, j; EXJSON *node; for ( i = 0; i < E_NUM_P(e); ++i ) {node = EV_VALUE_P(E_DATA_P(e) + i); for ( j = 0; j < E_NUM_P(node); ++j ) { v = E_DATA_P( node ) + j;
#define EXJSON_FOR_EACH_ARRAY_END() }}} while(0)
#define EXJSON_FOR_EACH_OBJECT_END() }} while(0)

void trash_file_fp(void *v);
void task_fork(char *command);
void main_thread();
void *http_thread(void *p);

#endif /* CKNIT_CK_TASK_H */

#pragma clang diagnostic pop